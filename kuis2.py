import shapefile #merupakan pyshp yang tadi telah diinstal
w=shapefile.Writer()  #mendeklarasikan file shapefile yang baru
w.shapeType #mengecek type dari writer yang dibuat diatas

w.field("kolom1","C") #membuat field dengan nama kolom1 dan dengan type character
w.field("kolom2","C") #membuat field dengan nama kolom2  dan dengan type character

w.record("levi","satu") #mengisi record dari field yang telah dibuatkan
w.record("eren","dua") #mengisi record dari field yang telah dibuatkan
w.record("saitama","tiga") #mengisi record dari field yang telah dibuatkan
w.record("wolfram","empat") #mengisi record dari field yang telah dibuatkan
w.record("aot","lima") #mengisi record dari field yang telah dibuatkan
w.record("onepunch","enam") #mengisi record dari field yang telah dibuatkan


#membuat POLYLINE ada 8\6 buah
w.poly(parts=[[[2,0], [7,0], [6,3]]],shapeType=shapefile.POLYLINE) #menggunakan shapetype polyline yang dimana titik koordinat pertama akan bertemu dengan ujung nya membentuk trapesium.
w.poly(parts=[[[-1,-5], [4,-1]]],shapeType=shapefile.POLYLINE) #menggunakan shapetype polyline yang dimana titik koordinat pertama akan bertemu dengan ujung nya membentuk trapesium.
w.poly(parts=[[[6,-2], [3,1], [-2,1]]],shapeType=shapefile.POLYLINE) #menggunakan shapetype polyline yang dimana titik koordinat pertama akan bertemu dengan ujung nya membentuk trapesium.
w.poly(parts=[[[8,0], [13,0], [12,6]]],shapeType=shapefile.POLYLINE) #menggunakan shapetype polyline yang dimana titik koordinat pertama akan bertemu dengan ujung nya membentuk trapesium.
w.poly(parts=[[[-4,-12], [6,-12], [3,-6]]],shapeType=shapefile.POLYLINE) #menggunakan shapetype polyline yang dimana titik koordinat pertama akan bertemu dengan membentuk trapesium.
w.poly(parts=[[[-16,-12],[-9,-6], [-13,-6]]],shapeType=shapefile.POLYLINE) #menggunakan shapetype polyline yang dimana titik koordinat pertama akan bertemu dengan ujung nya membentuk trapesium.

w.save("kuis2") #yang berada didalam kurung merupakn nama file yang telah/akan di save